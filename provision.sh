#!/usr/bin/env bash
set -e
sudo yum install -y centos-release-SCL
sudo yum install -y php54
grep -qi /opt/rh/php54/enable ~/.bashrc || cat <<SCL > ~/.bashrc

# SCL
[ -f /opt/rh/php54/enable ] && source /opt/rh/php54/enable

SCL
