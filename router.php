<?php
$static_regex = '/^\/(j|cs)s\//';
$data_regex = '/^\/data\//';
$uri = $_SERVER['REQUEST_URI'];
if (preg_match($static_regex, $uri)) {
  header('Location: ' . '/www' . $uri);
} elseif (preg_match($data_regex, $uri)) {
    // I'm foregoing the luxury of a nice framework
    header('Content-Type: application/json');
    // Not a huge fan of having to repeat this directory; should be a const
    $file_location = 'data/results/' . preg_replace($data_regex, '', $uri);
    try {
      if (!file_exists($file_location)) {
        throw new Exception;
      }
      // Push file directly to stdout so we don't read into a string
      readfile($file_location);
    } catch (Exception $e) {
      header('HTTP/1.0 404 Not found');
      echo '{"error":"No data found"}';
      return true;
    }

} else {
  return false;
}
// A long time ago, I learned not to close wholly-PHP files because it's
//   extra overhead for what the parser does naturally at EOF
