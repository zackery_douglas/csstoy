# CSS Toy
An experiment in doing something interesting with a CSS file.

By Zackery Douglas (Copyright 2014)

Let's try to generate a color palette from a CSS file, which we could later
use as part of a theme, and in its turn be fungible via a web interface to
create variadic derivative themes.

## Principle Technologies

 * PHP v5.4:
   Principally used for its simple HTTP server, PHP v5.4 gets the files on the
   server and affords a processing language.
   + Composer:
     The front-running dependency injection framework, with a myriad of
     available packages; one of which, Sabberworm PHP CSS Parser, was used to
     jumpstart development.
 * Vanilla JS:
   With no need for a library yet there's plenty of power available in modern
   browsers; including `document.querySelector`, `function.prototype.bind`,
   and more!
