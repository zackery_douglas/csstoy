<?php
/**
 * Process all uploaded files presented
 */
function processFiles($results_id) {
  $results = array();
  foreach(array_keys($_FILES) as $name) {
    $results[] = processPhpFile($_FILES[$name], $results_id);
  }
  storeResults($results, $results_id);
}

/**
 * Process an individual file upload
 * @argument $file An element from $_FILES
 * @argument $results_id An integer ID for this processing round
 */
function processPhpFile($file, $results_id) {
  $dir = sprintf(
    '%s/%s',
    'data/css',
    $results_id
  );
  if (!is_dir($dir)) {
    // @suppression is eval
    mkdir($dir);
  }
  // Use md5 to get a quick, safe, predictable hash
  $path = sprintf('%s/%s', $dir, md5($file['name']));
  if (!move_uploaded_file($file['tmp_name'], $path)) {
    throw new Exception;
  }
  return processCssFile($path);
}

/**
 * Process an individual CSS file off disk and into memory
 * TODO: use more filehandles / streams
 */
function processCssFile($path) {
  try {
    $size = filesize($path);
    // Not having to load the whole file into memory would be nice...
    $parser = new \Sabberworm\CSS\Parser(file_get_contents($path));
    $doc = $parser->parse();
    $cmpr_size = strlen(
        $doc->render(\Sabberworm\CSS\OutputFormat::createCompact())
    );
    $cmpr_savings = (1 - round($cmpr_size / $size, 3)) * 100;
    $selectors = $doc->getAllSelectors();
    $values = $doc->getAllValues();
    return array(
      'Number of Selectors' => count($selectors),
      'Number of Values' => count($values),
      'Compressed size' => $cmpr_size,
      'File size' => $size,
      'Compression savings' => $cmpr_savings,
    );
  } catch (Exception $e) {
    return array(
      'error' => 'Could not retrieve results',
    );
  }
}

/**
 * Store results to disk
 */
function storeResults($results, $results_id) {
  $dir = 'data/results';
  if (!is_dir($dir)) {
    mkdir($dir);
  }
  file_put_contents(sprintf('%s/%s', $dir, $results_id),
                    json_encode($results));
}
