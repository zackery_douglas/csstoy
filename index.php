<html>
<head>
  <title>CM CSS Toy ~ZDouglas</title>
</head>
<body>
  <div id="form">
    <form enctype="multipart/form-data" action="/upload.php" method="post">
      <ul id="files">
        <li><input type="file" name="css-0" placeholder="*.css" accept="text/css" autofocus required></input></li>
        <button type="button" id="addFile">+</button>
      </ul>
      <button name="processFiles" type="submit">POST</button>
    </form>
    <div id="css-template" style="display:none;">
      <input type="file" name="css-" placeholder="*.css" accept="text/css"></input>
    </div>
  </div>
  <div id="results">
    <ol id="results-list"></ol>
    <button type="button" id="reset">Reset</button>
  </div>
  <!-- <script src="/js/lodash.min.js"></script> -->
  <script src="/js/app.js"></script>
</body>
</html>