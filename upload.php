<?php
require 'vendor/autoload.php';
require 'processor.php';

// Huge practicality measure - this is just a toy, so we will track the ID of
//   our results with a text file.
$results_id = uniqid();

try {
  // Guard $_FILES for malevolence
  // (Cribbed-ish from PHP.net)
  foreach(array_keys($_FILES) as $name) {
    if (!isset($_FILES[$name]['error'])) {
      // Possible attack
      throw new Exception;
    }
    elseif ($_FILES[$name]['error'] != UPLOAD_ERR_OK) {
      // PHP-caught bad file
      throw new Exception;
    }
    elseif ($_FILES[$name]['size'] >= pow(2, 24)) {
      // File larger than ~16MB
      throw new Exception;
    }
    elseif (strpos($_FILES[$name]['name'], '/') > 0) {
      // I know of no browser sending '/' along with the file name
      throw new Exception;
    }
    elseif (substr($_FILES[$name]['name'], -4) != '.css') {
      // Probably not a CSS file
      // TODO: Should probably check for odd bytes like 0x0, too
      throw new Exception;
    }
  }
  processFiles($results_id);
} catch (Exception $e) {
  header('Location: ' . '/');
  return;
}

header('Location: ' . '/#r=' . $results_id);