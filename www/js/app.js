/**/;(function _init_4239875(window) {

var location = window.location;
var document = window.document;
var qs = document.querySelector.bind(window.document);
var ael = function (e, t, c) {
  document.addEventListener.call(e, t, c);
};
var form = document.forms[0];
window.ael = ael;
window.qs = qs;

// Branch action based on request
!/#r=\w+?/.test(location.href) ? function showForm () {
  // Display Form
  qs('#form').style.display='initial';
  qs('#results').style.display='none';

  // Attach listeners
  ael(qs('#addFile'), 'click', function () {
    var li = document.createElement('li');
    var file = qs('#css-template').firstElementChild.cloneNode(true);
    file.id += Math.random() * 100;
    li.appendChild(file);
    qs('#files').insertBefore(li, this);
  });
}() : function showResults () {
  // Display Results
  qs('#form').style.display='none';
  qs('#results').style.display='initial';

  // XHR for results JSON
  var list = qs('#results-list');
  var render = function (data, into) {
    var html = '';
    for (var id in data) {
      if (!data.hasOwnProperty(id)) {
        continue;
      }
      html += '<li id="file-' + id + '">' + renderData(data[id], id) + '</li>';
    }
    into.innerHTML = html;
  };
  var renderData = function (data, id) {
    var html = '<table id="data-' + id + '">';
    for (var property in data) {
      if (!data.hasOwnProperty(property)) {
        continue;
      }
      html += '<tr><th>' + property + '</th><td>' + data[property] + '</td></tr>';
    }
    html += '</table>';
    return html;
  };
  var onXhrLoad = function (event) {
    var data = JSON.parse(xhr.responseText);
    render(data, list);
  };
  var results_id = location.href.match(/=(\w+$)/)[1] || 0;
  var xhr = new XMLHttpRequest();
  xhr.onload = onXhrLoad;
  xhr.onerror = function () {
    alert('Could not retrieve #' + results_id);
  };
  xhr.open('get', '/data/' + results_id);
  xhr.send();
  ael(qs('#reset'), 'click', function () {
    location.href = '/';
  });
}();
}(window));
